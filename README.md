# Installed modules instructions
### Note: Do not do 'react-native link'

## Branch information:
1. master -> branch for base project
2. test_module -> branch for testing modules

## Instructions:
1. Clone the project
2. Delete .git file '**rm -rf .git**'
3. Initialise .git inside '**git init**'
4. Add remote '**git remote add origin <link-to-remote-repo>**'
5. Push upstream master '**git push -u origin master**'
6. Do not forget Gitflow (**NEVER** push directly to master)
7. **頑張って!**

## List of modules installed:
1. react-navigation
2. react-native-gesture-handler
3. react-native-firebase
4. react-native-maps